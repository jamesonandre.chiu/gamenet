﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
public class TakeDMG : MonoBehaviourPunCallbacks
{
    [SerializeField]
    Image healthBar;

    private float Max_HP = 100;
    public float HP;


    // Start is called before the first frame update
    void Start()
    {
        HP = Max_HP;
        healthBar.fillAmount = HP / Max_HP;
    }

    [PunRPC]
    public void takeDamage(int damage)
    {
        HP -= damage;
        Debug.Log("Cuurent HP: " + HP);

        healthBar.fillAmount = HP / Max_HP;


        if (HP <= 0)
        {
            Die();
        }
    }

    private void Die()
    {

        if(photonView.IsMine)
        {
            GameManager.instance.LeaveRoom();
        }
    }
}
