using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Target : MonoBehaviourPun
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [PunRPC]
    public void newTargetPosition()
    {

        Vector3 position = new Vector3(Random.Range(-23, 23), Random.Range(15, 25), Random.Range(20, 50));
       transform.position = position;
    }
}
