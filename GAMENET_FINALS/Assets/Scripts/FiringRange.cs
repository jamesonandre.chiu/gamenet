using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class FiringRange : MonoBehaviourPunCallbacks
{
    public GameObject target;
    public GameObject badTarget;
    public GameObject bonusTarget;

    private int RandomX;
    private int RandomY;
    private int RandomZ;
    Vector3 pos;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < 5;i++)
        {
            RandomX = Random.Range(-23,23);
            RandomY = Random.Range(15, 25);
            RandomZ = Random.Range(20, 50);

            pos = new Vector3(RandomX, RandomY, RandomZ);
            GameObject blueTarget = PhotonNetwork.Instantiate(target.name, pos, Quaternion.identity);
        }

        RandomX = Random.Range(-23, 23);
        RandomY = Random.Range(15, 25);
        RandomZ = Random.Range(20, 50);
        pos = new Vector3(RandomX, RandomY, RandomZ);
        GameObject redTarget = PhotonNetwork.Instantiate(badTarget.name, pos, Quaternion.identity);

        RandomX = Random.Range(-23, 23);
        RandomY = Random.Range(15, 25);
        RandomZ = Random.Range(20, 50);
        pos = new Vector3(RandomX, RandomY, RandomZ);
        GameObject yellowTarget = PhotonNetwork.Instantiate(bonusTarget.name, pos, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
