using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class Shooting : MonoBehaviourPunCallbacks
{

    [SerializeField]
    Camera fpsCamera;

    [SerializeField]
    public float fireRate = 0.1f;
    private float fireTimer = 0;

    [SerializeField] private int score = 0;
    [SerializeField] private int winScore = 200;
    [SerializeField] bool isWin = false;

    [Header("Targets")]
    public GameObject target;
    public GameObject badTarget;
    public GameObject bonusTarget;

    private int RandomX;
    private int RandomY;
    private int RandomZ;

    private GameObject blueTarget;
    private GameObject redTarget;
    private GameObject yellowTarget;

    Vector3 pos;

    void Start()
    {


        for (int i = 0; i < 5; i++)
        {
            RandomX = Random.Range(-23, 23);
            RandomY = Random.Range(15, 25);
            RandomZ = Random.Range(20, 50);

            pos = new Vector3(RandomX, RandomY, RandomZ);
            blueTarget = PhotonNetwork.Instantiate(target.name, pos, Quaternion.identity);
        }

        RandomX = Random.Range(-23, 23);
        RandomY = Random.Range(15, 25);
        RandomZ = Random.Range(20, 50);
        pos = new Vector3(RandomX, RandomY, RandomZ);

        redTarget = PhotonNetwork.Instantiate(badTarget.name, pos, Quaternion.identity);

        RandomX = Random.Range(-23, 23);
        RandomY = Random.Range(15, 25);
        RandomZ = Random.Range(20, 50);
        pos = new Vector3(RandomX, RandomY, RandomZ);

        yellowTarget = PhotonNetwork.Instantiate(bonusTarget.name, pos, Quaternion.identity);

        //scoreUI.GetComponent<Text>().text ="Points: " + score;
    }

    // Update is called once per frame
    void Update()
    {
        if (fireTimer < fireRate)
        {
            fireTimer += Time.deltaTime;
        }

        if (Input.GetButton("Fire1") && fireTimer > fireRate)
        {
            fire();
        }

       


    }

    public void fire()
    {
        fireTimer = 0.0f;
        Ray ray = fpsCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            Debug.Log(hit.collider.gameObject.name);

            if (hit.collider.gameObject.CompareTag("Target"))
            {
                this.GetComponent<PhotonView>().RPC("gainScore", RpcTarget.AllBuffered, 10);

                hit.collider.gameObject.GetComponent<PhotonView>().RPC("newTargetPosition", RpcTarget.AllBuffered);

    
                if (photonView.IsMine)
                {
                    GameObject scoreUI = GameManager.instance.score_UI;
                    scoreUI.GetComponent<Text>().text = "Points: " + score;

                    
                }

                if (this.score >= winScore)
                {
                    isWin = true;
                    photonView.RPC("Result", RpcTarget.AllBuffered);
                }

            }
            if (hit.collider.gameObject.CompareTag("BadTarget"))
            {
                this.GetComponent<PhotonView>().RPC("loseScore", RpcTarget.AllBuffered, 5);

                hit.collider.gameObject.GetComponent<PhotonView>().RPC("newTargetPosition", RpcTarget.AllBuffered);

            
                if (photonView.IsMine)
                {
                    GameObject scoreUI = GameManager.instance.score_UI;
                    scoreUI.GetComponent<Text>().text = "Points: " + score;
                }

                if (this.score >= winScore)
                {
                    isWin = true;
                    photonView.RPC("Result", RpcTarget.AllBuffered);
                }
            }
            if (hit.collider.gameObject.CompareTag("BonusTarget"))
            {
                this.GetComponent<PhotonView>().RPC("bonusScore", RpcTarget.AllBuffered, 2);

                hit.collider.gameObject.GetComponent<PhotonView>().RPC("newTargetPosition", RpcTarget.AllBuffered);


                if (photonView.IsMine)
                {
                    GameObject scoreUI = GameManager.instance.score_UI;
                    scoreUI.GetComponent<Text>().text = "Points: " + score;

                   
                }
                if (this.score >= winScore)
                {
                    isWin = true;
                    photonView.RPC("Result", RpcTarget.AllBuffered);
                }

            }
        }
    }

    [PunRPC]
    public void gainScore(int points)
    {
        score += points;
        Debug.Log("Score: " + score);

    }

    [PunRPC]
    public void loseScore(int points)
    {
        score -= points;
        Debug.Log("Score: " + score);

    }

    [PunRPC]
    public void bonusScore(int points)
    {
        score *= points;
        Debug.Log("Score: " + score);

       
        // StartCoroutine(newBonusTargetPosition());
    }

    [PunRPC]
    public void Result()
    {
        if (photonView.IsMine)
        {
            GameObject scoreUI = GameManager.instance.score_UI;
            scoreUI.GetComponent<Text>().text = "Points: " + score;
        }
        
        GameObject winner = GameManager.instance.result_UI;

        //this.gameObject.transform.GetComponent<PlayerMovement>().enabled = false;
        this.gameObject.GetComponent<Shooting>().enabled = false;

        Debug.Log("Endgame");

        if (isWin && photonView.IsMine)
        {
            winner.GetComponent<Text>().text = "You are the winner!";
            
            
        }
        else
        {
            winner.GetComponent<Text>().text = "You lose";

        }
        StartCoroutine(ExitRoom());

    }

    IEnumerator ExitRoom()
    {
        yield return new WaitForSeconds(3.0f);
        PhotonNetwork.LoadLevel("LobbyScene");
        PhotonNetwork.LeaveRoom(this);
       
       
    }
}
