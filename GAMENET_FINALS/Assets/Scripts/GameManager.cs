using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject playerPrefab;
    public Transform[] spawnPoints;
    public GameObject score_UI;
    public GameObject result_UI;

    public static GameManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        

        if (PhotonNetwork.IsConnected)
        {
            if (playerPrefab != null)
            {
               // object playerSelectionNumber;
               
                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = spawnPoints[actorNumber - 1].position;
                GameObject newPlayer = PhotonNetwork.Instantiate(playerPrefab.name, instantiatePosition, playerPrefab.transform.rotation);

               // PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(spawnPoints[RNG].transform.position.x, 0, spawnPoints[RNG].transform.position.z), Quaternion.identity);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
}
