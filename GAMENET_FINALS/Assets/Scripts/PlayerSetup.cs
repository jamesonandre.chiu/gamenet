using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks
{

    [SerializeField]
    private GameObject camera;


    // Start is called before the first frame update
    void Start()
    {
        if (photonView.IsMine)
        {
            transform.GetComponent<PlayerMovement>().enabled = true;
            camera.GetComponent<Camera>().enabled = true;
            GameManager.instance.score_UI = GameObject.Find("Score");
            GameManager.instance.result_UI = GameObject.Find("Winner");
        }
        else
        {
            transform.GetComponent<PlayerMovement>().enabled = false;
            camera.GetComponent<Camera>().enabled = false;

        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
