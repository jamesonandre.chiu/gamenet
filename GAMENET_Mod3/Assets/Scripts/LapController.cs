using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;


public class LapController : MonoBehaviourPunCallbacks
{
    public List<GameObject> lapTrigger = new List<GameObject>();
    public enum RaiseEventsCode
    {
        WhoFinishEventCode = 0
    }


    private int finishOrder = 0;

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;

    }
    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if(photonEvent.Code == (byte)RaiseEventsCode.WhoFinishEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfFinishplayer = (string)data[0];
            finishOrder = (int)data[1];
            int viewID = (int)data[2];

            Debug.Log(nickNameOfFinishplayer + " " + finishOrder);

            GameObject orderUIText = RacingGameManager.instance.finisherTextUI[finishOrder - 1];
            orderUIText.SetActive(true);

            if(viewID == photonView.ViewID) // IsYou
            {
                orderUIText.GetComponent<Text>().text = finishOrder + " " + nickNameOfFinishplayer + "(YOU)";
                orderUIText.GetComponent<Text>().color = Color.red;
            }
            else
            {
                orderUIText.GetComponent<Text>().text = finishOrder + " " + nickNameOfFinishplayer;
            }

           
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject go in RacingGameManager.instance.lapTrigger)
        {
            lapTrigger.Add(go); 
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if(lapTrigger.Contains(col.gameObject))
        {
            int indexOfTrigger = lapTrigger.IndexOf(col.gameObject);

            lapTrigger[indexOfTrigger].SetActive(false);

        }
        if(col.gameObject.tag == "FinishTrigger")
        {
            GameFinish(); 
        }
    }

    public void GameFinish()
    {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<VehicleMovementScript>().enabled = false;

        finishOrder++;

        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;
        
        //Event Data
        object[] data = new object[] { nickName, finishOrder,viewId };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };
   
        PhotonNetwork.RaiseEvent((byte) RaiseEventsCode.WhoFinishEventCode, data, raiseEventOptions,sendOptions);
    }

   
}
