using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;
using ExitGames.Client.Photon;




public class LaserShoot : MonoBehaviourPunCallbacks
{
    [Header("HP_Group")]
    public float maxHP = 100;
    [SerializeField] private float HP;
    public Image healthbar;
   // public Text diedTxt;
    
    public GameObject weaponNuzzle;

    public enum RaiseEventsCode
    {
        Died = 0,
        Winner = 1
    }

    private void Start()
    {
        HP = maxHP;
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0) && gameObject.GetComponent<PhotonView>().IsMine)
        {
            Fire();
            Debug.Log("Fire");
        }
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;

    }
    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
   
 void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventsCode.Died)
        {
            object[] data = (object[])photonEvent.CustomData;
           

            string KilledPlayer = (string)data[0];
            string killer = (string)data[1]; //SELF

            Debug.Log(killer + " " + KilledPlayer);

            GameObject killLogUIText = DeathRaceGameManager.instance.killLog_UI;
           //killLogUIText.SetActive(true);

            killLogUIText.GetComponent<Text>().text = killer + " killed " + KilledPlayer;
        }
        else if(photonEvent.Code == (byte)RaiseEventsCode.Winner)
        {
            object[] data = (object[])photonEvent.CustomData;

            string WinnerPlayer = (string)data[0];
            Debug.Log(WinnerPlayer);

            GameObject winUI = DeathRaceGameManager.instance.winner_UI;

            winUI.GetComponent<Text>().text = WinnerPlayer + " is the winner!!!";
        }
    }
  
    public void Fire()
    {
        RaycastHit hit;
        if (Physics.Raycast(weaponNuzzle.transform.position, weaponNuzzle.transform.forward, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
               
                Debug.Log("DMG");
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 10,photonView.Owner.NickName);

            }
        }
    }

    [PunRPC]
    public void TakeDamage(int dmg, string killer, PhotonMessageInfo info)
    {
        this.HP -= dmg;
        this.healthbar.fillAmount = HP / maxHP;

        Debug.Log(HP);

        if (HP <= 0)
        {
            Die(killer);

        }

      
    }

    public void Die(string killer)
    {
        Debug.Log("Dead");
        GetComponent<VehicleMovementScript>().isControlEnabled = false;
        DeathRaceGameManager.instance.playerList.Remove(this.gameObject);
        //Event Data
        object[] data = new object[] { photonView.Owner.NickName, killer };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.Died, data, raiseEventOptions, sendOptions);

        if (DeathRaceGameManager.instance.playerList.Count <= 1)
        {
            
            winner(killer);

        }

    }
   
    public void winner(string killer)
    {
        object[] data = new object[] { killer };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.Winner, data, raiseEventOptions, sendOptions);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            Debug.Log("DMG");
            this.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered,10, other.gameObject.GetComponent<Bullets>().shooter);
            Destroy(other.gameObject);
        }
    }
   
}

   