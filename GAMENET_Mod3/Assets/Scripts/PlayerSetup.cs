using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;
  //  public GameObject playerUiPrefab;
    [SerializeField] TextMeshProUGUI playerNameText;
   


    // Start is called before the first frame update
    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();
        
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<VehicleMovementScript>().enabled = photonView.IsMine;
            GetComponent<VehicleMovementScript>().isControlEnabled = false;
            GetComponent<LapController>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            playerNameText.text = photonView.Owner.NickName;
            GetComponent<VehicleMovementScript>().enabled = photonView.IsMine;
            GetComponent<VehicleMovementScript>().isControlEnabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
            DeathRaceGameManager.instance.playerList.Add(this.gameObject);
        }
    }

    
}
