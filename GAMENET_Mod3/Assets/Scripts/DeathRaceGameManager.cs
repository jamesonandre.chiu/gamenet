using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class DeathRaceGameManager : MonoBehaviour
{
    public GameObject[] vehiclePrefabs;
    public Transform[] spawnPointsPosition;
    public GameObject killLog_UI;
    public GameObject winner_UI;

    public List<GameObject> playerList = new List<GameObject>();

    public static DeathRaceGameManager instance = null;

    //public Text timeText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;
            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                Debug.Log((int)playerSelectionNumber);
            }
            int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
            Vector3 instantiatePosition = spawnPointsPosition[actorNumber - 1].position;
            GameObject newPlayer = PhotonNetwork.Instantiate(vehiclePrefabs[(int)playerSelectionNumber].name, instantiatePosition, vehiclePrefabs[(int)playerSelectionNumber].transform.rotation);
           
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
