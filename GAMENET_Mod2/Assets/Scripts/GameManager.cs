using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject[] spawnPoints;
    //public GameObject spawnB;

    public static GameManager instance;
    private int RNG;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if(PhotonNetwork.IsConnectedAndReady)
        {
            RNG = Random.Range(0, spawnPoints.Length);
            //int randomX = Random.Range(-10, 10);
            //int randomZ = Random.Range(-10, 10);
            PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(spawnPoints[RNG].transform.position.x, 0, spawnPoints[RNG].transform.position.z),Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void spawnLocation()
    {
        RNG = Random.Range(0, spawnPoints.Length);

        playerPrefab.transform.position = new Vector3(spawnPoints[RNG].transform.position.x, 0, spawnPoints[RNG].transform.position.z);

        //playerPrefab.transform.GetComponent<PlayerMovementController>().enabled = true;
    }
}
