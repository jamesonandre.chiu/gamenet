using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;
public class ShootingScript : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;

    [Header("HP_Group")]
    public float maxHP = 100;
    [SerializeField] private float HP;
    public Image healthbar;

    [Header("Name")]
    [SerializeField] TextMeshProUGUI playerTextName;

    [SerializeField] int score = 0;
    [SerializeField] Text UI_score;

    [SerializeField] bool isEndGame = false;

    private int winScore = 10;
    private Animator animator;
    
    void Start()
    {
        HP = maxHP;
        healthbar.fillAmount = HP / maxHP;
        animator = this.GetComponent<Animator>();
        
        playerTextName.text = photonView.Owner.NickName;

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);

            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if(hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 10);

                if (!hit.collider.gameObject.GetComponent<Animator>().GetBool("isDead"))
                {
                    updateScore(hit);
                }
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int dmg, PhotonMessageInfo info)
    {
        this.HP -= dmg;
        this.healthbar.fillAmount = HP / maxHP;

        if (HP <= 0)
        {
            Die();
            //Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
            StartCoroutine(DisplayKillLog(info));
            
        }
        
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 pos)
    {
        GameObject hitEffectGameObejct = Instantiate(hitEffectPrefab, pos, Quaternion.identity);
        Destroy(hitEffectGameObejct, 0.15f);    
    }

    public void Die()
    {
        if(photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("Respawn_txt");
        float respawnTime = 5.0f;
         
        while (respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are killed. Respawning in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        int RNG = Random.Range(0, GameManager.instance.spawnPoints.Length);
        GameObject newLocation = GameManager.instance.spawnPoints[RNG];


        /*int randomPointX = Random.Range(-20, 20);
        int randomPointZ = Random.Range(-20, 20);*/
        this.transform.position = new Vector3(newLocation.transform.position.x, 0, newLocation.transform.position.z);
        transform.GetComponent<PlayerMovementController>().enabled = true;
        photonView.RPC("RegainHP", RpcTarget.AllBuffered);

    }

    IEnumerator DisplayKillLog(PhotonMessageInfo info)
    {
        GameObject killLog = GameObject.Find("Kill_Log");
        float displayTimer = 5;
        

        while (displayTimer > 0)
        {
            yield return new WaitForSeconds(1.0f);
            displayTimer--;

          
            killLog.GetComponent<Text>().text = info.Sender.NickName + " killed " + info.photonView.Owner.NickName;
           
        }
        killLog.GetComponent<Text>().text = "";
        
    }

    [PunRPC]
    public void RegainHP()
    {
       
        HP = 100;
        healthbar.fillAmount = HP / maxHP;
      
    }
    
    public void updateScore(RaycastHit hit)
    {
        GameObject scoreCount = GameObject.Find("Kills_Score");

        if (hit.collider.gameObject.GetComponent<ShootingScript>().HP <= 0)
        {
            this.score += 1;
            scoreCount.GetComponent<Text>().text = "Kill: " + score.ToString();
        }
        if (this.score >= winScore)
        {
            photonView.RPC("Result", RpcTarget.AllBuffered);
        }
    }

    [PunRPC]
    public void Result(PhotonMessageInfo info)
    {
        GameObject winner = GameObject.Find("EndGameText");
        isEndGame = true;
        //transform.GetComponent<PlayerMovementController>().enabled = false;
        if (isEndGame)
        {
            this.transform.GetComponent<PlayerMovementController>().enabled = false;
            if (this.score >= winScore)
            {
                winner.GetComponent<Text>().text = "You are the champion!";
            }
            else
            {
                winner.GetComponent<Text>().text = PhotonNetwork.NickName + " is the champion!";
            }
        }
        
        StartCoroutine(ExitRoom());
    }
    IEnumerator ExitRoom()
    {
        yield return new WaitForSeconds(3.0f);
        PhotonNetwork.LoadLevel("LobbyScene");
        PhotonNetwork.LeaveRoom(this);
    }


}

